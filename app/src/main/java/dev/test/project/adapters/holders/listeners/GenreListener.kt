package dev.test.project.adapters.holders.listeners

import dev.test.project.items.Genre

interface GenreListener {

    /**
     * Клик по жанру
     * @param item жанр
     */
    fun onGenreClick(item: Genre)
}