package dev.test.project.adapters.holders.listeners

import dev.test.project.items.Movie

interface MovieListener {

    /**
     * Клик по фильму
     * @param item фильм
     */
    fun onMovieClick(item: Movie)

    /**
     * Клик по кнопке добаваления в избранное
     * @param item фильм
     */
    fun onFavoriteClick(item: Movie)
}