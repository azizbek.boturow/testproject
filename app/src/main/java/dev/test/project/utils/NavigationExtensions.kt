package dev.test.project.utils

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

/**
 * Методы для перекидывания результата работы фрагмента
 */
fun <T> Fragment.getNavigationResult(key: String) =
    findNavController().currentBackStackEntry?.savedStateHandle?.get<T>(key)

fun <T> Fragment.setNavigationResult(key: String, result: T) {
    findNavController().previousBackStackEntry?.savedStateHandle?.set(key, result)
}