package dev.test.project.utils

/**
 * Константы-ключи (bundle, extra)
 */
const val MOVIE_BUNDLE_KEY = "movie"
const val MOVIE_RESULT_KEY = "result_movie"