package dev.test.project.items

import com.google.gson.annotations.SerializedName

/**
 * POJO для списка фильмов и жанров
 */
data class MoviesObject(
    @SerializedName("films")
    var movies: List<Movie>,
    var genres: List<Genre>
)