package dev.test.project.items

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * POJO для жанра
 */
@Parcelize
data class Genre (
    var genre: String
): Parcelable